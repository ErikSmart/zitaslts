package com.example.zitaslts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sincuenta.setOnClickListener {
            val intento = Intent(this, registro::class.java)
            startActivity(intento)

        }
        ingresar.setOnClickListener{
            val ingresar = Intent(this, MenuActivity::class.java)
            startActivity(ingresar)
        }
    }
}
